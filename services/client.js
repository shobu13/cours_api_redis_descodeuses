const redis = require('redis')
const {promisify} = require('util')
const accessBdd = redis.createClient()

function sendToRedis(clientHotel) {
    let asyncHset = promisify(accessBdd.hset).bind(accessBdd)
    return asyncHset(`client:${clientHotel.identifiantClient}`,
        "identifiantClient", clientHotel.identifiantClient,
        "nom", clientHotel.nom,
        "prenom", clientHotel.prenom,
        "addresse", clientHotel.addresse,
        "email", clientHotel.email)
}

function getFromRedis(identifiantClient) {
    let asyncHgetAll = promisify(accessBdd.hgetall).bind(accessBdd)
    return asyncHgetAll(`client:${identifiantClient}`)
}

exports.sendToRedis = sendToRedis
exports.getFromRedis = getFromRedis
