const {Chambre} = require('./models/chambre')
const {Client} = require('./models/client')
const {Reservation} = require('./models/reservation')
const utils = require('./utils')
const serviceClient = require('./services/client')

let run = async () => {
    let clientHotel = new Client(utils.uuidv4(), 'lelu', 'awenn', '11 rue charle de gaule', 'test@test.test')

    /*serviceClient.sendToRedis(clientHotel).then(
        (result) => {
            console.log(result)
            console.log(`le client ${clientHotel.nom} ${clientHotel.prenom} à bien été envoyé à redis.`)
        }).catch(
        (err) => {
            console.log(err)
        })*/

    /*let promise = serviceClient.sendToRedis(clientHotel)
    promise.then((result) => {
        //faire des trucs
    })
    promise.catch((err) => {
        //faire des trucs
    })
    promise.finally(() => {
        //faire des trucs
    })*/

    await serviceClient.sendToRedis(clientHotel)


    let clientFromRedis = await serviceClient.getFromRedis(clientHotel.identifiantClient)
    console.log(clientFromRedis)
}


console.log("début du fil d'exécution")
let n = 0
run().then(() => console.log("exec finie"))
n += 1
console.log("fin du premier fil d'execution")
